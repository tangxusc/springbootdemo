package com.example.springboot.demo.SpringBootDemo.repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.springboot.demo.SpringBootDemo.Author;

public interface AuthorRepository extends CrudRepository<Author, Long> {

}
