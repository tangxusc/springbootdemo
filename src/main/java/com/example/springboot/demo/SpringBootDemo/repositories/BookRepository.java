package com.example.springboot.demo.SpringBootDemo.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.springboot.demo.SpringBootDemo.Book;

public interface BookRepository extends CrudRepository<Book, Long> {

}
